About
=====

CloudPuppet is a set of scripts to help you run a scalable and configurable
masterless Puppet setup, with an example of using Amazon's S3 service for
secure file storage.

You can use it to run a dynamic Puppet setup in the cloud, where you don't
particularly want to have a Puppetmaster as a dependency/bottleneck, and to
avoid the pain of certificate management.

This project is opinionated about having properly versioned puppet modules and
the separation of configuration from the puppet modules.  Don't complain, just
do it.

How it works
============

The idea is to present a simple wget'able script that can be downloaded and
executed on the node that needs to be configured, which will in turn configure
the system for Puppet, extract the node configuration from the script itself (a
data blob), download the Puppet modules from another URL, and then execute
Puppet.  Afterwards, you could upload or send the report to a centralised
system.

The assumption is that this script is stored in a secure location, since this
would be the place to put all the secrets for your applications.

1. generate a tarball of your puppet recipes
2. upload the tarball to S3 and generate a signed URL for it
3. write your application and site specific configuration in either puppet or ENC YAML format
3. generate a script which contains the application specific configuration, using the above signed URL
4. upload the script to S3 and generate a signed URL for it
5. as part of your image, userdata, or cloud formation configuration, download and execute this URL.


Frequently Asked Questions
==========================

Why put the node configuration in the script?
    Both the script and the configuration are node/application specific and tightly bound to one
    another, so why not?

Why version Puppet recipes?  I already have them versioned in git.
    Trust me, just do it.

Why not version node configuration too?
    You should version this yourself.  However, configuration might change over time, so it's
    reasonable to allow this to be changed after-the-fact.  If you want to handle changes
    another way, please do.

Why not put the node configuration in the boot/userdata directly?
    If you find that flexible enough for your needs, you can do that too.


Example Usage
=============

Assuming you are storing your puppet modules under ./myapp/::

    $ ./tarball_from_git_tags.sh myapp
    Creating /home/user/cloudpuppet/pkgs/myapp-20120620.tgz
    $ s3cmd put s3://mybucket/puppet/modules/myapp-20120620.tgz /home/user/cloudpuppet/pkgs/myapp-20120620.tgz
    $ SIGNED_URL=`s3cmd signurl s3://mybucket/puppet/modules/myapp-20120620.tgz 1500000000`
    $ vim myapp_us_west.pp
    $ ./make_script.sh "$SIGNED_URL" templates/debian.template > myapp_us_west.sh
    $ s3cmd put s3://mybucket/puppet/scripts/myapp_us_west.sh myapp_us_west.sh
    $ s3cmd signurl s3://mybucket/puppet/scripts/myapp_us_west.sh 1500000000
    http://mybucket.s3.amazonaws.com/puppet/scripts/myapp_us_west.sh?AWSAccessKey=aaa&Expires=1500000000&Signature=bbb

You could then add the following to a boot script::

    wget -q -O- "http://mybucket.s3.amazonaws.com/puppet/scripts/myapp_us_west.sh?AWSAccessKey=aaa&Expires=1500000000&Signature=bbb" | bash -

Example Client Run
==================

::

    # wget -q -O- "http://your-s3-bucket/puppet/roles/webserver.sh?awskey=lakjsdfljk" | bash -
    :: Installing puppet
    :: Installing wget
    :: Downloading puppet modules
    :: Extracting puppet modules
    :: Extracting node config
    :: Running puppet
    puppet output here
    :: Uploading puppet report to S3

