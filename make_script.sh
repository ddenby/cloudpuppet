#!/bin/bash

DEFAULT_TEMPLATE_FILE="debian.template"
if [ -e "defaults.config" ]; then
  . defaults.config
fi

MODULE_URL="$1"
CONFIG_FILE="$2"
TEMPLATE_FILE="$3"

if [ -z "$CONFIG_FILE" ]; then
  echo "Usage: $0 <module url> <config file> [template file]"
  exit 1
fi

[ -n "$TEMPLATE_FILE" ] || TEMPLATE_FILE="$DEFAULT_TEMPLATE_FILE"

cat "$TEMPLATE_FILE" | sed "s|__MODULE_URL__|$MODULE_URL|g"
cat "$CONFIG_FILE"
