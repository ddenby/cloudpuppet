#!/bin/bash

# 
# Create a tarball for each tag in the named git repository,
# and place it in pkgs/
#

GITPATH="$1"
ONLY_TAG="$2"

if [ ! -d "$GITPATH" -o ! -d "$GITPATH/.git" ]; then
    echo "Syntax: $0 <path to git repo> [tag to build]"
    exit 1
fi

mkdir -p pkgs
tarball_prefix="$PWD/pkgs/`basename "$GITPATH"`"
cd $GITPATH
tags=`git tag`

if [ -z "$tags" ]; then
    echo "No tags found in $GITPATH. Nothing to do."
    exit 1
fi

for tag in $tags; do
    tarball="$tarball_prefix-$tag.tgz"

    if [ -n "$ONLY_TAG" -a "$tag" != "$ONLY_TAG" ]; then
        continue
    fi

    if [ -e "$tarball" ]; then
        continue
    fi

    echo "Creating $tarball"
    git archive --prefix="modules/" "$tag" | gzip > "$tarball" || rm -f "$tarball"
done

